<?php 

include"conexion.php";


if(isset($_POST['enviar'])){



    $titulo= $_POST['titulo'];
    $autor= $_POST['autor'];
    $año= $_POST['año'];
    $idioma= $_POST['idioma'];

    $agregar="INSERT INTO libro (titulo,autor,publicacionaño,idioma) VALUES ('$titulo','$autor','$año','$idioma')";

    $resultado= pg_query($conexion,$agregar);
    if(!$resultado){
	    echo '<script>
        alert("Error");
        '.header("Refresh:0; url=index.php").'
	    </script>';
        }else{
	        echo '<script>
         alert("Agregado exitosamente");
         '.header("Refresh:0; url=index.php").'

	        </script>';
    }
}


?>

<!DOCTYPE html>
<html>
<head>
	<title>Ingresar Libro</title>
	<meta charset="utf-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="EstiloIngresar.css">
</head>
<body>

    <div class="container">
        <div class="row justify-content-center pt-5 mt-5 mr-1">
            <div class="col-md-4 formulario">
			<form class="" action="" method="post">
				<h1 class="text-center">Ingresar libro</h1>
                    <div class="form-group text-align mx-sm-4 pt-3">
                        <input type="text" class="form-control" id="titulo" aria-describedby="###" placeholder="Titulo de libro" name="titulo" id="titulo">
                    </div>
                    <div class="form-group text-align mx-sm-4 pt-3">
                        <input type="text" class="form-control" id="autor" aria-describedby="###" placeholder="Nombre de autor" name="autor" id="autor">
                    </div>
                    <div class="form-group text-align mx-sm-4 pt-3">
                        <input type="text" class="form-control" id="año" aria-describedby="###" placeholder="Año de publicación" name="año" id="año">
                    </div>
                    <div class="form-group text-align mx-sm-4 pt-3">
                        <input type="text" class="form-control" id="idioma" aria-describedby="###" placeholder="Idioma" name="idioma" id="idioma">
                    </div>
                    <div class="form-group mx-sm-4 pt-2">
                        <button type="submit" class="btn btn-success float-left" name="enviar" id="btn">Ingresar</button>
                        <button type="button" class="btn btn-danger float-right" onclick="history.back()">Cancelar</button>
                    </div>
			</form>
	        </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    <script src="validacion.js"></script>
</body>
</html>