<?php 

	include"conexion.php";

	


$consulta2="SELECT COUNT(*) AS total_registro FROM libro";
	$sql_registro= pg_query($conexion,$consulta2); /*Total de todos los registros*/
	$result_registro = pg_fetch_array($sql_registro);
	$total_registro= $result_registro['total_registro'];
	$por_pagina=5; /*Cantidad de filas por paginado*/

if(empty($_GET['pagina'])){

	$pagina=1;
}else{
	$pagina=$_GET['pagina'];
}

$desde=($pagina-1)*$por_pagina;
$total_paginas= ceil($total_registro/$por_pagina); 

$consulta= "SELECT * FROM libro
limit $por_pagina offset $desde; ";
$resultado= pg_query($conexion,$consulta);
 ?>



<!DOCTYPE html>
<html>
<head>
	<title>Actividad 1</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/9cc978e9bd.js" crossorigin="anonymous"></script>
</head>
<body>
<h1 class="text-center py-4">Crud de Libros</h1>

<div class="container">

<a href="Ingresar.php" class="btn btn-success my-2">Ingresar Libro</a>


</div>

<div class="container">
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Titulo</th>
      <th scope="col">Nombre Autor</th>
      <th scope="col">Año</th>
      <th scope="col">Idioma</th>
       <th scope="col">Modificar</th>
      <th scope="col">Eliminar</th>
    </tr>
  </thead>
  <tbody>
  	<?php 

		

		while($mostrar=pg_fetch_array($resultado)){
		 ?>
    <tr>
    	
      <th scope="row"><?php echo $mostrar['idlibro'] ?></th>
      <td><?php echo $mostrar['titulo'] ?></td>
      <td><?php echo $mostrar['autor'] ?></td>
      <td><?php echo $mostrar['publicacionaño'] ?></td>
      <td><?php echo $mostrar['idioma'] ?></td>
      <td>
      	<a href="modificar.php?id=<?php echo $mostrar['idlibro'] ?>" class="btn btn-success">Modificar</a>
      </td>
      <td>
      <a href="Eliminar.php?id=<?php echo $mostrar['idlibro'] ?>" class="btn btn-danger" onclick="return confirm('¿Estas seguro?');">Eliminar</a>
        
      </td>
     
     
    </tr>
 	<?php 
	}
		 ?>
  </tbody>
</table>


	<nav aria-label="Page navigation example" >
  <ul class="pagination">
    <li class="page-item"><a class="page-link" href="#">Previous</a></li>
<?php
    for ($i=1; $i <=$total_paginas ; $i++) { 

			if ($i == $pagina) {
				echo '<li class="page-link">'.$i.'</li>';
				
			}else{
				echo '<li class="page-item"><a class="page-link" href="?pagina='.$i.'">'.$i.'</a></li>';
			

			}
		
		}
?>
    
  
    <li class="page-item"><a class="page-link" href="#">Next</a></li>
  </ul>
</nav>
</div>


<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</body>
</html>