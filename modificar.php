<?php 
include"conexion.php";



if(!empty($_POST)){

$idlibro=$_POST['idlibro'];
$titulo= $_POST['titulo'];
$autor= $_POST['autor'];
$año= $_POST['año'];
$idioma= $_POST['idioma'];

if(isset($_POST['enviar'])){

	
	$consulta= pg_query($conexion, "UPDATE libro
	SET titulo='$titulo', autor='$autor', publicacionaño='$año', idioma='$idioma'
	 WHERE idlibro=$idlibro");

if(!$consulta){
echo '<script>
	alert("Error");
	'.header("Refresh:0; url=index.php").'
	</script>';
}else{
	echo '<script>
	alert("Registro actualizado exitosamete");
	'.header("Refresh:0; url=index.php").'
	</script>';
}

}


}

if(empty($_GET['id'])){
	
	header('Location: index.php');
}
$id=$_GET['id'];
$sql= pg_query($conexion,"SELECT*FROM libro WHERE idlibro=$id");

$resul=pg_num_rows($sql);
	
	if($resul ==0){
	header('Location: index.php');
}else{
		while($data = pg_fetch_array($sql)){

		$idlibro= $data['idlibro'];
		$titulo= $data['titulo'];
		$autor= $data['autor'];
		$publicacionaño= $data['publicacionaño'];
		$idioma= $data['idioma'];


	}
	


}
 ?>

 <!DOCTYPE html>
<html>
<head>
	<title>Modificar Libro</title>
	<meta charset="utf-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="EstiloIngresar.css">
</head>
<body>

    <div class="container">
        <div class="row justify-content-center pt-5 mt-5 mr-1">
            <div class="col-md-4 formulario">
			<form class="" action="" method="post">
				<h1 class=" h1 text-center text-white">Modificar libro</h1>
                    <div class="form-group text-align mx-sm-4 pt-3">
                    	<input type="hidden" name="idlibro" value="<?php echo $idlibro; ?>">	
                        <input type="text" class="form-control" id="titulo" aria-describedby="###" placeholder="Titulo de libro" name="titulo" id="titulo" value="<?php echo $titulo; ?>">
                    </div>
                    <div class="form-group text-align mx-sm-4 pt-3">
                        <input type="text" class="form-control" id="autor" aria-describedby="###" placeholder="Nombre de autor" name="autor" id="autor" value="<?php echo $autor; ?>">
                    </div>
                    <div class="form-group text-align mx-sm-4 pt-3">
                        <input type="text" class="form-control" id="año" aria-describedby="###" placeholder="Año de publicación" name="año" id="año" value="<?php echo $publicacionaño; ?>">
                    </div>
                    <div class="form-group text-align mx-sm-4 pt-3">
                        <input type="text" class="form-control" id="idioma" aria-describedby="###" placeholder="Idioma" name="idioma" id="idioma" value="<?php echo $idioma; ?>">
                    </div>
                    <div class="form-group mx-sm-4 pt-2">
                        <button type="submit" class="btn btn-success float-left" name="enviar" id="btn">Modificar</button>
                        <button type="button" class="btn btn-danger float-right" name="cancelar"  onclick="history.back()">Cancelar</button>
                    </div>
			</form>
	        </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    <script src="validacion.js"></script>
</body>
</html>

 